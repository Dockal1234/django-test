from django.utils import timezone
from datetime import timedelta

# Create your tests here.

from django.test import TestCase

from .models import Domain, DomainFlag

class DomainDetailViewTestCase(TestCase):
    '''domain detail test'''

    def setUp(self):
        self.domain = Domain.objects.create(
            fdqn='www.test.cz',
            created=timezone.now()
        )
        '''flag not yet valid'''
        self.flag_1 = DomainFlag.objects.create(
          type='EX',
          domain_id=self.domain.pk,
          valid_from=timezone.now()+ timedelta(days=1),
        )
        '''flag after validation'''
        self.flag_2 = DomainFlag.objects.create(
            type='EX',
            domain_id=self.domain.pk,
            valid_to=timezone.now() - timedelta(days=1)
        )
        '''valid flag'''
        self.flag_3 = DomainFlag.objects.create(
            type='EX',
            domain_id=self.domain.pk,
            valid_from=timezone.now() - timedelta(days=1)
        )

    def test_active_flags(self):
        '''test if flags active'''
        self.assertTrue(self.flag_1 not in self.domain.active_flags())
        self.assertTrue(self.flag_2 not in self.domain.active_flags())
        self.assertTrue(self.flag_3 in self.domain.active_flags())


