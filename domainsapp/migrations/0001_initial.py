# Generated by Django 2.2.12 on 2020-04-20 19:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Domain',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('fdqn', models.CharField(max_length=255)),
                ('crdate', models.DateTimeField()),
                ('erdate', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'domain',
            },
        ),
        migrations.CreateModel(
            name='DomainFlag',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('flag', models.TextField(blank=True, null=True)),
                ('valid_from', models.DateTimeField(blank=True, null=True)),
                ('valid_to', models.DateTimeField(blank=True, null=True)),
                ('domain', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='domainsapp.Domain')),
            ],
            options={
                'db_table': 'domain_flag',
            },
        ),
    ]
