from django.apps import AppConfig


class DomainsappConfig(AppConfig):
    name = 'domainsapp'
