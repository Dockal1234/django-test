from django.urls import path
from .views import DomainDetailView, DomainsListView
from . import views

urlpatterns = [
    # ex: /domainsapp/
    path('', views.index, name='index'),
    # ex: /domainsapp/domain
    path('domain/<int:pk>', DomainDetailView.as_view(), name='detail'),
    path('domain/', DomainsListView.as_view(), name='domain')

]