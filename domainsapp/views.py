from django.shortcuts import get_object_or_404, render
from .models import Domain, DomainFlag
from django.http import HttpResponse
from django.views.generic import DetailView, ListView
from domainsapp.models import Domain
from django.utils import timezone


def index(request):
    domains_list = Domain.objects.order_by('id')[:5]
    output = ', '.join([d.fdqn for d in domains_list])
    return HttpResponse(output)

class DomainDetailView(DetailView):
    model = Domain
    template_name = 'domainsapp/detail.html'


class DomainsListView(ListView):
    queryset = Domain.objects.filter(deleted__isnull = True)
    context_object_name = 'active_domains'
    template_name = 'domainsapp/domain.html'

