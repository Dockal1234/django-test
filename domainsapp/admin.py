from django.contrib import admin

from .models import Domain, DomainFlag

class DomainAdmin(admin.ModelAdmin):
    list_display = ('fdqn', 'created', 'expiration', 'deleted')

class DomainFlagAdmin(admin.ModelAdmin):
    fields = ('type', 'domain', 'valid_from', 'valid_to')
    list_display = ('type_verbose', 'domain', 'valid_from', 'valid_to')
    date_hierarchy = 'valid_to'



admin.site.register(Domain, DomainAdmin)
admin.site.register(DomainFlag, DomainFlagAdmin)
