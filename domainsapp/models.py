from django.utils import timezone
from django.db import models

class Domain(models.Model):
    fdqn = models.CharField(max_length=50)
    created = models.DateTimeField()
    expiration = models.DateTimeField(blank=True, null=True)
    deleted = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.fdqn

    # returns domain's active flags
    def active_flags(self):
        return self.domainflag_set.filter(valid_from__lt = timezone.now()).filter(valid_to__gt = timezone.now())

    class Meta:
        db_table = 'domain'

class DomainFlag(models.Model):

    domain = models.ForeignKey(Domain, null=True, on_delete=models.SET_NULL)

    TYPE_CHOICES = (
        ('EX', 'Expired'),
        ('OZ', 'Outzone'),
        ('DC', 'Delete candidate')
    )
    type = models.CharField(max_length=2, choices=TYPE_CHOICES, default='EX')
    valid_from = models.DateTimeField(blank=True, null=True)
    valid_to = models.DateTimeField(blank=True, null=True)

    def type_verbose(self):
        return dict(DomainFlag.TYPE_CHOICES)[self.type]

    def __str__(self):
        return self.type

    class Meta:
        db_table = 'domain_flag'
